﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AnimalAnimator : MonoBehaviour
{
    public Animator _animator;
    [System.NonSerialized]
    public Vector3 _rootMotionPositionDelta;
    [System.NonSerialized]
    public Quaternion _rootMotionRotationDelta;
    public PlayerController PLAYERCONTROLLER;

    public void Awake()
    {
        _animator = gameObject.GetComponent<Animator>();
    }

    public void FixedUpdate()
    {
        UpdateState();
    }

    public void UpdateState()
    {
        _animator.SetFloat(CharacterAnimatorParamId.HorizontalSpeed, PLAYERCONTROLLER._InputVectorValue);


        float jumpSpeed = PLAYERCONTROLLER.MovementSettings.JumpSpeed;
        float normVerticalSpeed = PLAYERCONTROLLER.VerticalVelocity.y.Remap(-jumpSpeed, jumpSpeed, -1.0f, 1.0f);
        _animator.SetFloat(CharacterAnimatorParamId.VerticalSpeed, normVerticalSpeed, .05f , Time.deltaTime);

        _animator.SetBool(CharacterAnimatorParamId.IsGrounded, PLAYERCONTROLLER.Motor.GroundingStatus.IsStableOnGround);
    }


}
