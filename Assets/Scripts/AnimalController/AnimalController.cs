﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AnimalController : MonoBehaviour
{
    public AnimalPhysics _AnimalPhysics;
    public AnimalAnimator _AnimalAnimator;
    public GameObject AnimalMesh;


    public void Awake()
    {
        _AnimalPhysics = GetComponent<AnimalPhysics>();
        _AnimalAnimator = GetComponent<AnimalAnimator>();

    }

}
