﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class CharacterAnimatorParamId
{
    public static readonly int HorizontalSpeed = Animator.StringToHash("HorizontalSpeed");
    public static readonly int VerticalSpeed = Animator.StringToHash("VerticalSpeed");
    public static readonly int IsGrounded = Animator.StringToHash("IsGrounded");
}
