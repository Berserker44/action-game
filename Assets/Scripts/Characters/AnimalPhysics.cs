﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AnimalPhysics : MonoBehaviour
{
    //[System.NonSerialized]
    public Vector3 _rootMotionPositionDelta;
    //[System.NonSerialized]
    public Quaternion _rootMotionRotationDelta;
    public AnimalAnimator _AnimalAnimator;

    // Start is called before the first frame update
    void Awake()
    {
        _AnimalAnimator = GetComponent<AnimalAnimator>();
        PlayerController.ClearRootMotionData += ClearRootMotionData;
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnAnimatorMove()
    {

        _rootMotionPositionDelta += _AnimalAnimator._animator.deltaPosition;
        _rootMotionRotationDelta = _AnimalAnimator._animator.deltaRotation * _rootMotionRotationDelta;
    }

    public void ClearRootMotionData(PlayerController PC)
    {
        _rootMotionPositionDelta = Vector3.zero;
        _rootMotionRotationDelta = Quaternion.identity;

    }
}
