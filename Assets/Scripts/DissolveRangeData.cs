﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct DissolveRangeData
{
    //[System.NonSerialized]
    public float Start;
    //[System.NonSerialized]
    public float End;
}
