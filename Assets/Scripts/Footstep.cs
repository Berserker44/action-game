﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Footstep : MonoBehaviour
{

    Collider[] HitColliders;
    Transform ThisTransform;
    public float CheckRadius;
    public bool StepWait;
    public bool StepDetected = false;
    public Color GizmoColor;
    public GameObject FootStepParticle;
    public StepState stepState;
    public StepState PrevStepState;

    //temp var
    public GameObject ReferenceObject;
    public LayerMask StepLayerMask;

    // Start is called before the first frame update
    void Awake()
    {
        ThisTransform = this.transform;
    }

    // Update is called once per frame
    void Update()
    {
        if (ReferenceObject.activeSelf)
        {
            CheckStep();
        }
    }


    void OnStepStay()
    {
        PrevStepState = stepState;
        stepState = StepState.StepStay;
        GizmoColor = Color.yellow;

    }

    private void OnStepEnter(Collider[] collision)
    {
        GizmoColor = Color.red;

        PrevStepState = stepState;
        stepState = StepState.StepEnter;
        foreach (Collider col in collision)
        {

            PlayParticle(ThisTransform.position);
            print(col.name);

        }
    }



    private void OnStepExit()
    {
        PrevStepState = stepState;
        stepState = StepState.StepExit;
        GizmoColor = Color.green;
    }

    public void CheckStep()
    {

        HitColliders = Physics.OverlapSphere(ThisTransform.position, CheckRadius, StepLayerMask);

        StepDetected = false;
        foreach (Collider col in HitColliders)
        {

                StepDetected = true;

        }


        if (StepDetected)
        {


            if (PrevStepState != StepState.StepEnter && stepState != StepState.StepStay)
            {
                OnStepEnter(HitColliders);
            }
            else
            {

                OnStepStay();
            }


        }
        else
        {
            if (stepState == StepState.StepExit)
            {
                stepState = StepState.StepNoContact;

            }
            else
            {
                OnStepExit();
            }


        }


    }

    public void PlayParticle(Vector3 Point)
    {
        GameObject DustParticle = Instantiate(FootStepParticle, Point, Quaternion.identity);
        DustParticle.SetActive(true);
    }

    void OnDrawGizmos()
    {
        // Draw a yellow sphere at the transform's position
        Gizmos.color = GizmoColor;
        Gizmos.DrawSphere(transform.position, CheckRadius);
    }

    IEnumerator WaitForStep()
    {
        StepWait = true;
        yield return .2f;
        StepWait = false;
    }
}
