﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;




public class MoveMorphe_Test : MonoBehaviour
{
    public float speed;
    public bool transitioning;
    Sequence RootMotionValueSequence;
    Sequence DissolveSequence;

    public MeshMorpher MeshMorph;
    public List<GameObject> MeshObjects;
    public DissolveRangeData[] DissolveRange;
    public Transform[] RingActivatePoint;
    public float TargetMorphValue;
    public float CurrentMorphValue;
    public float transitiontime = 1;
    bool TimeSlow;
    float CurrentTime = 1;
    public ParticleSystem GlowParticle;
    public GameObject OldAnimal;
    public GameObject NewAnimal;
    int CurrPlayer = 0;
    int PrevPlayer = 1;
    public Material DissolveMaterial;
    public float CurrentDissolveValue;
    float TargetDissolveValue;

    public GameObject RingParticle;
    public GameObject OuterRingParticle;

    // Start is called before the first frame update
    void Start()
    {
        SetupDissolveMaterial();
        MeshObjects[1].SetActive(false);

    }

    public void SetupDissolveMaterial()
    {

        //Enable proper mask keyword
        DissolveMaterial.DisableKeyword("_DISSOLVEMASK_PLANE");
        DissolveMaterial.DisableKeyword("_DISSOLVEMASK_SPHERE");
        DissolveMaterial.DisableKeyword("_DISSOLVEMASK_BOX");
        DissolveMaterial.DisableKeyword("_DISSOLVEMASK_CYLINDER");
        DissolveMaterial.DisableKeyword("_DISSOLVEMASK_CONE");
        DissolveMaterial.EnableKeyword("_DISSOLVEMASK_XYZ_AXIS");
        //For material editor to select proper mask name inside dropdown
        DissolveMaterial.SetFloat("_DissolveMask", 1);
        DissolveMaterial.EnableKeyword("_DISSOLVEMASK_XYZ_AXIS");

    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetMouseButtonDown(0))
        {


        }

        if (transitioning)
        {
            MeshMorph.MorphValue = CurrentMorphValue;
            UpdateDissolveMaterial();
        }

        if (Input.GetMouseButtonDown(1) || Input.GetButtonDown("StartButton"))
        {
            TimeSlowToggle();

        }


    }

    IEnumerator SwitchPlayerPhysics()
    {
        //DOTween.KillAll();
        // DissolveSequence.Kill(false);
        // RootMotionValueSequence.Kill(false);
        // DissolveSequence = DOTween.Sequence();
        // RootMotionValueSequence = DOTween.Sequence();

        GlowParticle.Stop();

        DOTween.Kill("Value1");
        DOTween.Kill("Value2");
        DOTween.Kill("Value3");

        //print("is the DissolveSequence tween active? =" + RootMotionValueSequence.IsActive());

        ActivateRingParticle();
        transitioning = true;
        print(" Dissolve is currently = " + DissolveMaterial.GetFloat("_DissolveMaskOffset"));

        CurrentDissolveValue = DissolveRange[CurrPlayer].Start;
        print("Dissolve needs to be = " + DissolveRange[CurrPlayer].Start + " , but Dissolve is currently = " + DissolveMaterial.GetFloat("_DissolveMaskOffset"));
        DOTween.To(() => CurrentDissolveValue, x => CurrentDissolveValue = x, DissolveRange[CurrPlayer].End, 0.25f).SetId("Value1"); ;
        print("Dissolve needs to be = " + DissolveRange[CurrPlayer].Start + " , but Dissolve is currently = " + DissolveMaterial.GetFloat("_DissolveMaskOffset"));

        CurrentMorphValue = MeshMorph.MorphValue;

        yield return new WaitForSeconds( 0.25f);
        OldAnimal.SetActive(false);
        DOTween.To(() => CurrentMorphValue, x => CurrentMorphValue = x, TargetMorphValue, transitiontime).SetEase(Ease.InSine).SetId("Value2"); ;
        yield return new WaitForSeconds(transitiontime);
        GlowParticle.Play();
        DOTween.To(() => CurrentDissolveValue, x => CurrentDissolveValue = x, DissolveRange[CurrPlayer].Start, transitiontime + 1).SetId("Value3"); 
        NewAnimal.SetActive(true);
        yield return new WaitForSeconds(transitiontime);
        yield return new WaitForSeconds( 1);
        transitioning = false;


    }

    public void ActivateRingParticle()
    {
        GameObject SpawnParticle = Instantiate(RingParticle, RingActivatePoint[CurrPlayer].position, Quaternion.identity);

        ParticleSystem PS = SpawnParticle.GetComponent<ParticleSystem>();
        ParticleSystemRenderer PSR = PS.GetComponent<ParticleSystemRenderer>();
        Material PSM = PSR.material;
        PSM.DOFloat(.15f, "_DissolveMaskOffset", 1.5f).SetDelay(0f);
        SpawnParticle.transform.parent = RingActivatePoint[CurrPlayer];
        SpawnParticle.SetActive(true);

    }

    public void UpdateDissolveMaterial()
    {
        DissolveMaterial.SetFloat("_DissolveMaskOffset", CurrentDissolveValue);
    }

    public void SwitchTargetValue(int PlayerUpdate)
    {

            TargetMorphValue = PlayerUpdate;
            PrevPlayer = CurrPlayer;
            CurrPlayer = PlayerUpdate;
            OldAnimal = MeshObjects[PrevPlayer];
            NewAnimal = MeshObjects[PlayerUpdate];

        StopCoroutine("SwitchPlayerPhysics");
        StartCoroutine("SwitchPlayerPhysics");
    }

    public void TimeSlowToggle()
    {
        TimeSlow = !TimeSlow;


        if (TimeSlow)
        {
            CurrentTime = .25f;

        }

        else
        {

            CurrentTime = 1f;

        }

        Time.timeScale = CurrentTime;
        Time.fixedDeltaTime = 0.02F * Time.timeScale;
    }
    
}
