﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class MovementSettings
{
    public float Acceleration = 25.0f; // In meters/second
    public float Decceleration = 25.0f; // In meters/second
    public float MaxHorizontalSpeed = 8.0f; // In meters/second
    public float JumpSpeed = 10.0f; // In meters/second
    public float JumpAbortSpeed = 10.0f; // In meters/second
}

[System.Serializable]
public class GravitySettings
{
    public float Gravity = 20.0f; // Gravity applied when the player is airborne
    public float GroundedGravity = 5.0f; // A constant gravity that is applied when the player is grounded
    public float MaxFallSpeed = 40.0f; // The max speed at which the player can fall
}
