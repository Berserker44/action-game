﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimator : MonoBehaviour
{

        public Animator _animator;
        public PlayerController _character;
    private PlayerInput playerInput;
    public float animationDamp = .1f;

    private void Awake()
        {
            _animator = GetComponent<Animator>();
            //_character = GetComponent<PlayerController>();
        }

        public void Update()
        {
            UpdateState();
        }

        public void UpdateState()
        {
            _animator.SetFloat(CharacterAnimatorParamId.HorizontalSpeed, _character._InputVectorValue);

            float jumpSpeed = _character.JumpUpSpeed;
           // float normVerticalSpeed = _character.Motor.Velocity.y.Remap(-jumpSpeed, jumpSpeed, -1.0f, 1.0f);
           // _animator.SetFloat(CharacterAnimatorParamId.VerticalSpeed, normVerticalSpeed);

           // _animator.SetBool(CharacterAnimatorParamId.IsGrounded, _character.Motor.GroundingStatus.IsStableOnGround);
        }
        

    public void PlayerJump()
    {
        _animator.SetTrigger("Jump");

    }
}
