﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class PlayerManager : MonoBehaviour
{

    public List<AnimalController> _AnimalController;
    public List<Transform> AnimatorObject;
    public AnimalController CurrentAnimalController;
    public AnimalController PreviousAnimalController;
    public PlayerController playerController;
    int CurrPlayer = 0;
    Transform thisTransform;
    IEnumerator IESwitchPlayerPhysics;
    Sequence RootMotionValueSequence;
    public RootMotionData PreviousRootMotionData;
    public RootMotionData CurrentRootMotionData;
    public MoveMorphe_Test MorphManagerTest;
    public float PreviousRootMotionMultiplier = 1;
    public float CurrentRootMotionMultiplier = 0;


    // Start is called before the first frame update
    void Awake()
    {
        thisTransform = transform;
        CurrentAnimalController = _AnimalController[0];
        if (_AnimalController.Count >= 1)
        {

            PreviousAnimalController = _AnimalController[1];

        }
       // PreviousAnimalController.AnimalMesh.SetActive(false);
        //CurrentAnimalController.AnimalMesh.SetActive(true);
        // SwitchCharacter();

    }

    public void SwitchCharacter()
    {
        print("Starting Player Switch");

        for (int i = 0; i < _AnimalController.Count; i++)
        {
            if (i == CurrPlayer)
            {
                //_AnimalController[i].gameObject.SetActive(true);
                playerController.CurrentAnimal = CurrPlayer;
                //AnimatorObject[i].DOScale(new Vector3(1, 1, 1), 1);
            }

            else
            {
                //_AnimalController[i].gameObject.SetActive(false);
                //AnimatorObject[i].DOScale(new Vector3(0, 0, 0), 1);

            }
        }

        StartCoroutine("SwitchPlayerPhysics");
    }




    IEnumerator SwitchPlayerPhysics()
    {

        Debug.Log("Switching " + PreviousAnimalController.name + " to " + CurrentAnimalController.name);
        RootMotionValueSequence.Kill(true);
       // print ( "is the Rootmotiontween tween active? =" +RootMotionValueSequence.IsActive());
        playerController.MorphingToNewCharacter = true;
        PreviousRootMotionMultiplier = 1;
        CurrentRootMotionMultiplier = 0;
        RootMotionValueSequence = DOTween.Sequence();
        RootMotionValueSequence.Append(DOTween.To(() => PreviousRootMotionMultiplier, x => PreviousRootMotionMultiplier = x, 0, 2));
        RootMotionValueSequence.Join(DOTween.To(() => CurrentRootMotionMultiplier, x => CurrentRootMotionMultiplier = x, 1, 2));
        yield return new WaitForSeconds(2f);
        playerController.MorphingToNewCharacter = false;

        //PreviousAnimalController.AnimalMesh.SetActive(false);
        //CurrentAnimalController.AnimalMesh.SetActive(true);

    }

    // Update is called once per frame
    void Update()
    {
        InputUpdate();
    }

    public RootMotionData CombineRootMotion( RootMotionData PlayerRootMotionData )
    {
        PreviousRootMotionData._rootMotionPositionDelta = PreviousAnimalController._AnimalPhysics._rootMotionPositionDelta * PreviousRootMotionMultiplier;
        CurrentRootMotionData._rootMotionPositionDelta = CurrentAnimalController._AnimalPhysics._rootMotionPositionDelta * CurrentRootMotionMultiplier;
        PlayerRootMotionData._rootMotionPositionDelta = CurrentRootMotionData._rootMotionPositionDelta + PreviousRootMotionData._rootMotionPositionDelta;
        PlayerRootMotionData._rootMotionRotationDelta = CurrentAnimalController._AnimalPhysics._rootMotionRotationDelta;

        return PlayerRootMotionData;
    }

    public void InputUpdate()
    {
        if (Input.GetButtonDown("ShoulderRight") || Input.GetMouseButtonDown(0))
        {
            print("ShoulderRight was hit");
            PreviousAnimalController = _AnimalController[CurrPlayer];
            if (CurrPlayer >= _AnimalController.Count-1)
            {
                CurrPlayer = 0;
            }
            else
            {
                CurrPlayer++;
            }
            CurrentAnimalController = _AnimalController[CurrPlayer];
            SwitchCharacter();
            MorphManagerTest.SwitchTargetValue(CurrPlayer);

        }
        else 

        if (Input.GetButtonDown("ShoulderLeft"))
        {
            print("ShoulderLeft was hit");
            PreviousAnimalController = _AnimalController[CurrPlayer];

            if (CurrPlayer == 0)
            {
                CurrPlayer = _AnimalController.Count - 1;
            }
            else
            {
                CurrPlayer--;
            }
            CurrentAnimalController = _AnimalController[CurrPlayer];
            SwitchCharacter();
            MorphManagerTest.SwitchTargetValue(CurrPlayer);

        }

    }


}
