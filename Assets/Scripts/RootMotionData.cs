﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct RootMotionData
{
    //[System.NonSerialized]
    public Vector3 _rootMotionPositionDelta;
    //[System.NonSerialized]
    public Quaternion _rootMotionRotationDelta;
}
